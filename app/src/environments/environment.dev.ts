
export const environment = {
	production: true,
	name: 'desarrollo',
	apiUrl: 'http://10.10.0.16/backend/public/index.php/api/',
	dirImgsSubidas: 'http://10.10.0.16/backend/public/subidos/',
//	pyApiUrl: 'http://10.10.0.16:5005/api/',
	solpedURL: 'http://10.10.0.7:3005/api/',
	recepcionProductoUrl: 'http://10.10.0.7:3003/api/',
	AdminAlmacenesUrl: 'http://10.10.0.7:3004/api/',

	admCatalogoUrl: 'http://10.10.0.7:3006/api/adm/',
	configUrl: 'http://10.10.0.7:3006/api/config/',
	generalesUrl: 'http://10.10.0.7:3006/api/generales/',
	comprasUrl: 'http://10.10.0.7:3006/api/compras/',
};
