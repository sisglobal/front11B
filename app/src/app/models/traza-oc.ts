export interface TrazaOc {
	idTrazaOC?, 
	fechaAlta?, 
	justificacion?, 
	idComprasOC?, 
	idEstadoOC?, 
	estadoActual?,
	idSegUsuario?, 
	estadoAnterior?
}
